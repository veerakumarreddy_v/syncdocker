mkdir /local_pg_db
docker pull vreddyjazeera/syncservice:syncdb8
docker pull vreddyjazeera/syncservice:mpos-sync3
docker pull vreddyjazeera/syncservice:sync-api3
docker stop mpos-sync local_pg_db syncapi
docker rm mpos-sync local_pg_db syncapi
docker network rm sync
docker network create sync
docker run -d  -p 5000:5000 --restart unless-stopped --net sync --name mpos-sync vreddyjazeera/syncservice:mpos-sync3
docker run -d -p 8989:5432 -e POSTGRES_PASSWORD=Test1234 -e POSTGRES_USER=postgres -e POSTGRES_DB=mpos_db -e ENV_STORE_ID=$ENV_STORE_ID -v /local_pg_db=/var/lib/postgre/data --net sync --restart unless-stopped  --name local_pg_db vreddyjazeera/syncservice:syncdb8
sleep 20s
docker exec -it local_pg_db sh dump.sh
docker run -d -p 6341:6341 -e ENV_STORE_ID=$ENV_STORE_ID --net sync --restart unless-stopped --name syncapi vreddyjazeera/syncservice:sync-api3

