TRUNCATE TABLE sync_source;
TRUNCATE TABLE sync_table;

INSERT INTO sync_source (id,"name",host,port,username,"password",db,updated_by,updated_on) VALUES ('STAGING','staging','database-1.cw34ebrphxxg.eu-central-1.rds.amazonaws.com','5432','mpos_pre_prod_user','jazeera@1234','mpos_pre_prod_db','system','2019-10-09 15:42:26.444');

INSERT INTO sync_source (id,"name",host,port,username,"password",db,updated_by,updated_on) VALUES ('road','road','local_pg_db','5432','postgres','Test1234','mpos_db','system','2019-10-09 15:42:26.444');

INSERT INTO sync_table(id,source_id,target_id,source_table,target_table,source_pk,target_pk,sync_column,cond,active,group_on,last_update) VALUES ('STAGING_road_SYNC_SOURCE','STAGING','road','sync_source','sync_source','id','id','updated_on','( id = ''road'' OR id = ''STAGNING'')',FALSE,'road','2019-10-24T00:00:00');

INSERT INTO sync_table(id,source_id,target_id,source_table,target_table,source_pk,target_pk,sync_column,cond,active,group_on,last_update) VALUES ('STAGING_road_SYNC_TABLE','STAGING','road','sync_table','sync_table','id','id','updated_on','( source_id = ''road'' OR target_id = ''road'')',FALSE,'road','2019-10-24T00:00:00');
