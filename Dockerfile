FROM node:11
#COPY sync-api /sync-api
COPY mpos-api /mpos-api
COPY script.sh /script.sh
EXPOSE 5000
#EXPOSE 6341
CMD sh /script.sh
